package repositories.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Address;
import repositories.IRepository;
import repositories.impl.builders.IEntityBuilder;
import unitofwork.IUnitOfWork;

public class AddressRepository 
	extends Repository<Address>{

	protected AddressRepository(Connection connection,
			IEntityBuilder<Address> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}

	@Override
	protected void setUpUpdateQuery(Address entity) throws SQLException {
		update.setString(1, entity.getCountry());
		update.setString(2, entity.getCity());
		update.setString(3, entity.getPostalCode());
		update.setString(4, entity.getCity());
		update.setString(5, entity.getHouseNumber());
		update.setString(6, entity.getLocalNumber());
		update.setInt(7, entity.getId());
	}

	@Override
	protected void setUpInsertQuery(Address entity) throws SQLException {
		update.setString(1, entity.getCountry());
		update.setString(2, entity.getCity());
		update.setString(3, entity.getPostalCode());
		update.setString(4, entity.getCity());
		update.setString(5, entity.getHouseNumber());
		update.setString(6, entity.getLocalNumber());
		update.setInt(7, entity.getId());
	}

	@Override
	protected String getTableName() {
		return "address";
	}

	@Override
	protected String getUpdateQuery() {
		return "update address set (country,city,postalCode,city,houseNumber,localNumber)=(?,?,?,?,?,?)"
				+ "where id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into address(country,city,postalCode,city,houseNumber,localNumber) values(?,?,?,?,?,?)";
	}
	
}