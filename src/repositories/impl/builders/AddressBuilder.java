package repositories.impl.builders;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Address;


public class AddressBuilder implements IEntityBuilder<Address>{

	@Override
	public Address build(ResultSet rs) throws SQLException {
		Address address = new Address();
		address.setCountry(rs.getString("country"));
		address.setCity(rs.getString("city"));
		address.setPostalCode(rs.getString("postalCode"));
		address.setStreet(rs.getString("street"));
		address.setHouseNumber(rs.getString("houseNumber"));
		address.setLocalNumber(rs.getString("localNumber"));
		address.setId(rs.getInt("id"));
		return address;
	}

}
