package repositories.impl.builders;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Person;

public class PersonBuilder implements IEntityBuilder<Person>{

	@Override
	public Person build(ResultSet rs) throws SQLException {
		Person person = new Person();
		person.setFirstName(rs.getString("name"));
		person.setSecondName(rs.getString("sname"));
		person.setSurname(rs.getString("surname"));
		person.setPesel(rs.getString("pesel"));
		person.setNip(rs.getString("nip"));
		person.setId(rs.getInt("id"));
		return person;
	}

}
