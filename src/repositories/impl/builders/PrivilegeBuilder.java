package repositories.impl.builders;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.Privilege;

public class PrivilegeBuilder implements IEntityBuilder<Privilege>{

	@Override
	public Privilege build(ResultSet rs) throws SQLException {
		Privilege privilege = new Privilege();
		privilege.setName(rs.getString("name"));
		privilege.setId(rs.getInt("id"));
		return privilege;
	}

}
