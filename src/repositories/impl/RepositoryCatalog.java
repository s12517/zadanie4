package repositories.impl;

import java.sql.Connection;

import domain.Address;
import domain.Person;
import domain.Privilege;
import domain.Role;
import repositories.IRepository;
import repositories.IRepositoryCatalog;
import repositories.IUserRepository;
import repositories.impl.builders.AddressBuilder;
import repositories.impl.builders.PersonBuilder;
import repositories.impl.builders.PrivilegeBuilder;
import repositories.impl.builders.RoleBuilder;
import repositories.impl.builders.UserBuilder;
import unitofwork.IUnitOfWork;

public class RepositoryCatalog implements IRepositoryCatalog{

	private Connection connection;
	private IUnitOfWork uow;
	
	public RepositoryCatalog(Connection connection, IUnitOfWork uow) {
		super();
		this.connection = connection;
		this.uow = uow;
	}

	@Override
	public IUserRepository getUsers() {
		return new UserRepository(connection, new UserBuilder(), uow);
	}

	@Override
	public IRepository<Person> getPersons() {
		return new PersonRepository(connection, new PersonBuilder(), uow);
	}

	@Override
	public IRepository<Address> getAddresses() {
		return new AddressRepository(connection, new AddressBuilder(), uow);
	}

	@Override
	public IRepository<Privilege> getPrivileges() {
		return new PrivilegeRepository(connection, new PrivilegeBuilder(), uow);
	}

	@Override
	public IRepository<Role> getRoles() {
		return new RoleRepository(connection, new RoleBuilder(), uow);
	}

	@Override
	public void commit() {
		uow.commit();
	}

}
