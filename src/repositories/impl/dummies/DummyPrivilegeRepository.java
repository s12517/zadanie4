package repositories.impl.dummies;

import java.util.List;

import domain.Privilege;
import repositories.IRepository;

public class DummyPrivilegeRepository implements IRepository<Privilege>{

	DummyDb db;
	
	public DummyPrivilegeRepository(DummyDb db) {
	
	}

	
	public void save(Privilege entity) {
		
		db.privileges.add(entity);
	}

	@Override
	public void update(Privilege entity) {
		
	}

	@Override
	public void delete(Privilege entity) {
		db.privileges.remove(entity);
		
	}

	@Override
	public Privilege get(int id) {
		for(Privilege p : db.privileges)
			if(p.getId()==id)
				return p;
		return null;
	}

	@Override
	public List<Privilege> getAll() {

		return db.privileges;
	}

}