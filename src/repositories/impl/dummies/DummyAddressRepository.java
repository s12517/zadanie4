package repositories.impl.dummies;

import java.util.List;

import domain.Address;
import repositories.IRepository;

public class DummyAddressRepository implements IRepository<Address>{

	private DummyDb db;
	
	public DummyAddressRepository(DummyDb db) {
		this.db = db;
	}

	@Override
	public void save(Address entity) {

		db.addresses.add(entity);
		
	}

	@Override
	public void update(Address entity) {
		
	}

	@Override
	public void delete(Address entity) {

		db.addresses.remove(entity);
	}

	@Override
	public Address get(int id) {

		for(Address a: db.addresses)
			if(a.getId()==id)
				return a;
		return null;
	}

	@Override
	public List<Address> getAll() {
		return db.addresses;
	}

}
