package repositories.impl.dummies;

import java.util.*;

import domain.*;

public class DummyDb {

	List<Person> persons = new ArrayList<Person>();
	List<Address> addresses = new ArrayList<Address>();
	List<User> users = new ArrayList<User>();
	List<Role> roles = new ArrayList<Role>();
	List<Privilege> privileges = new ArrayList<Privilege>();
}
