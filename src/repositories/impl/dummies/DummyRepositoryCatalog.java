package repositories.impl.dummies;

import domain.Person;
import domain.Privilege;
import domain.Role;
import domain.Address;
import repositories.IRepository;
import repositories.IRepositoryCatalog;
import repositories.IUserRepository;

public class DummyRepositoryCatalog implements IRepositoryCatalog{

	private DummyDb db = new DummyDb();
	
	@Override
	public IUserRepository getUsers() {
		return new DummyUserRepository(db);
	}

	@Override
	public IRepository<Person> getPersons() {
		return new DummyPersonRepository(db);
	}

	@Override
	public IRepository<Role> getRoles() {
		return new DummyRoleRepository(db);
	}


	@Override
	public IRepository<Privilege> getPrivileges() {
		return null;
	}

	@Override
	public IRepository<Address> getAddresses() {
		return null;
	}

	@Override
	public void commit() {
				
	}
}
