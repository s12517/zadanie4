package repositories.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Role;
import repositories.IRepository;
import repositories.impl.builders.IEntityBuilder;
import unitofwork.IUnitOfWork;

public class RoleRepository 
	extends Repository<Role>{

	protected RoleRepository(Connection connection,
			IEntityBuilder<Role> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}

	@Override
	protected void setUpUpdateQuery(Role entity) throws SQLException {
		update.setString(1, entity.getName());
		update.setInt(2, entity.getId());
	}

	@Override
	protected void setUpInsertQuery(Role entity) throws SQLException {
		update.setString(1, entity.getName());
		update.setInt(2, entity.getId());
	}

	@Override
	protected String getTableName() {
		return "role";
	}

	@Override
	protected String getUpdateQuery() {
		return "update role set (name)=(?)"
				+ "where id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into role(name) values(?)";
	}
	
}