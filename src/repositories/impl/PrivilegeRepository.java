package repositories.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Privilege;
import repositories.IRepository;
import repositories.impl.builders.IEntityBuilder;
import unitofwork.IUnitOfWork;

public class PrivilegeRepository 
	extends Repository<Privilege>{

	protected PrivilegeRepository(Connection connection,
			IEntityBuilder<Privilege> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}

	@Override
	protected void setUpUpdateQuery(Privilege entity) throws SQLException {
		update.setString(1, entity.getName());
		update.setInt(2, entity.getId());
	}

	@Override
	protected void setUpInsertQuery(Privilege entity) throws SQLException {
		update.setString(1, entity.getName());
		update.setInt(2, entity.getId());
	}

	@Override
	protected String getTableName() {
		return "privilege";
	}

	@Override
	protected String getUpdateQuery() {
		return "update privilege set (name)=(?)"
				+ "where id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into privilege(name) values(?)";
	}
	
}