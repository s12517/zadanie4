package repositories.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Person;
import repositories.IRepository;
import repositories.impl.builders.IEntityBuilder;
import unitofwork.IUnitOfWork;

public class PersonRepository 
	extends Repository<Person>{

	protected PersonRepository(Connection connection,
			IEntityBuilder<Person> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}

	@Override
	protected void setUpUpdateQuery(Person entity) throws SQLException {
		update.setString(1, entity.getFirstName());
		update.setString(2, entity.getSecondName());
		update.setString(3, entity.getSurname());
		update.setString(4, entity.getPesel());
		update.setString(5, entity.getNip());
		update.setInt(6, entity.getId());
	}

	@Override
	protected void setUpInsertQuery(Person entity) throws SQLException {
		update.setString(1, entity.getFirstName());
		update.setString(2, entity.getSecondName());
		update.setString(3, entity.getSurname());
		update.setString(4, entity.getPesel());
		update.setString(5, entity.getNip());
		update.setInt(6, entity.getId());
	}

	@Override
	protected String getTableName() {
		return "person";
	}

	@Override
	protected String getUpdateQuery() {
		return "update person set (name, sname,surname,pesel,nip)=(?,?,?,?,?)"
				+ "where id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into person(name,sname,surname,pesel,nip) values(?,?,?,?,?)";
	}
	
}